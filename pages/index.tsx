import MainContainer from "../components/MainContainer";
import { filmData } from "../components/config";
import { Box, Container, Heading, Text } from "@chakra-ui/layout";
import Link from "next/link";

import { QueryClient, useQuery } from "react-query";
import { dehydrate } from "react-query/hydration";
import { getData } from "../api";
import { GetStaticProps, NextPage } from "next";

const Index: NextPage = () => {
  const films = filmData;
  const { data } = useQuery(["movieData", { id: "399566" }], getData);
  console.log(data);
  return (
    <MainContainer>
      <Heading>Film List</Heading>
      <Box>
        {films.map((film) => (
          <Text key={film.id}>
            <Link href={`/movie/${film.id}`}>
              <a>{film.title}</a>
            </Link>
          </Text>
        ))}
      </Box>
    </MainContainer>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const queryClient = new QueryClient();

  await queryClient.prefetchQuery(["movieData", { id: "399566" }], getData);

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
};

export default Index;

// export async function getStaticPaths() {
//   const res = await fetch('https://.../posts')
//   const posts = await res.json()

//   const paths = posts.map((post) => ({
//     params: { id: post.id },
//   }))

//   return { paths, fallback: true }
// }

// export async function getStaticProps({ params }) {
//   const res = await fetch(`https://.../posts/${params.id}`)
//   const post = await res.json()

//   return { props: { post } }
// }
