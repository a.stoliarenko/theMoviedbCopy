import { ChakraProvider, CSSReset } from "@chakra-ui/react";
import type { AppProps } from "next/app";
import { QueryClient, QueryClientProvider } from "react-query";
import theme from "../styles/threme/index";
import { ReactQueryDevtools } from "react-query/devtools";
import { getData } from "../api";
import NextNProgress from "nextjs-progressbar";
import React, { useState } from "react";
import { Hydrate } from "react-query/hydration";

function MyApp({ Component, pageProps }: AppProps) {
  // const queryClient = new QueryClient({
  //   defaultOptions: {
  //     queries: {
  //       queryFn: getData,
  //       refetchOnWindowFocus: false,
  //     },
  //   },
  // });
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        queryFn: getData,
        staleTime: Infinity,
        refetchOnWindowFocus: false,
      },
    },
  });
  return (
    <ChakraProvider theme={theme}>
      <QueryClientProvider client={queryClient}>
        <CSSReset />
        <Hydrate state={pageProps.dehydratedState}>
          <NextNProgress color='#29D' startPosition={0.3} height={3} />
          <Component {...pageProps} />
        </Hydrate>
        <ReactQueryDevtools initialIsOpen />
      </QueryClientProvider>
    </ChakraProvider>
  );
}
export default MyApp;

// theme={theme}
