import {CastType, ImageType, VideoType} from "../types/types";

type arraySliceFunctionType = (array: Array<CastType | VideoType | ImageType>, value: number) =>
    Array<CastType | VideoType | ImageType>

export const arraySlice: arraySliceFunctionType = (array, value) =>
    array.length > value ? array.slice(0, value) : array

