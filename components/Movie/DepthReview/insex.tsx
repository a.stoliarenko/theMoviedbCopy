import { Box, Container, Flex, StackDivider, VStack } from "@chakra-ui/layout";
import { FC, memo } from "react";
import WallInfo from "../WallInfo";
import CastCrewContainer from "./CastCrew";
import MediaContainer from "./Media";
import Recomendations from "./Recomendations";
import SocialContainer from "./Social/Social";

const MovieContent: FC = memo(() => {
  return (
    <Box w='100%'>
      <Container maxW='container.xl' p={10}>
        <Flex>
          <VStack
            w='80%'
            divider={<StackDivider borderColor='gray.800' />}
            spacing={10}
            align='stretch'
          >
            <CastCrewContainer />

            <SocialContainer />

            <MediaContainer />

            <Recomendations />
          </VStack>

          <Box pl='2%'>
            <WallInfo />
          </Box>
        </Flex>
      </Container>
    </Box>
  );
});

export default MovieContent;
