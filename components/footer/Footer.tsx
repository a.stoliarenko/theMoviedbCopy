import React, { FC, memo } from "react";
import {
    Box,
    Container,
    Flex,
    Heading
} from "@chakra-ui/layout";
import { Button } from "@chakra-ui/button";
import LinksCreator from "./LinksCreator";
import { Image } from "@chakra-ui/image";
import { dataFooter, bottomLogo } from "../config";

type PropsType = {
  title: string;
  subtypes: Array<any>;
};

const FooterColumn: FC<PropsType> = memo(({ title, subtypes }) => (
    <Box>
      <Heading fontSize='22'>{title.toUpperCase()}</Heading>
      <LinksCreator data={subtypes} />
    </Box>
));

const Footer: FC = memo(() => {
  const userNameInitialState: string = "Artur";

  const links = dataFooter.map((item, index) => (
      <FooterColumn key={index} title={item.title} subtypes={item.subtypes} />
  ));

  return (
      <Box as='footer' bgColor='brand.100' color='#fff'>
        <Container maxW='container.md' py='20'>
          <Flex justify='space-between'>
            <Box>
              <Image
                  src={bottomLogo}
                  alt='bottom logo'
                  htmlWidth='130px'
                  htmlHeight='94px'
              />
              <Button
                  py='2'
                  px='4'
                  color='#18bbe6'
                  bg='white'
                  _hover={{ bg: "white" }}
                  fontSize='18'
                  mt='5'
              >{`Hi ${userNameInitialState.toLowerCase()}!`}</Button>
            </Box>
            {links}
          </Flex>
        </Container>
      </Box>
  );
});

export default Footer;
